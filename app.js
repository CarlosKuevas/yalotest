
var express = require('express');
var request = require('request');
var moment = require('moment');
var app = express();

app.get('/', function (req, res) {
  let lastObservedPrice = '', dateOfLastObservation = '';
  request.get({
    url: 'https://bravenewcoin-v1.p.rapidapi.com/ticker?show=usd&coin=btc',
    headers: {'x-rapidapi-host': 'bravenewcoin-v1.p.rapidapi.com', 'x-rapidapi-key': 'c6f54aeb94msh523c3970f98d988p10f941jsn97fbb003c8cf'},
    json: true,
  }, (errR, resR, bodyR) => {
    if (bodyR && bodyR.last_price && bodyR.time_stamp) {
      lastObservedPrice = parseFloat(bodyR.last_price);
      lastObservedPrice = lastObservedPrice.toFixed(2);
      bodyR.time_stamp = parseInt(bodyR.time_stamp);
      dateOfLastObservation = moment.unix(bodyR.time_stamp).format("DD/MM/YYYY");
      res.status(200).json({
        code: 200,
        data: {
          error: false,
          values: {
            lastObservedPrice: lastObservedPrice,
            dateOfLastObservation: dateOfLastObservation
          } 
        }
      });
    } else {
      res.status(500).json({
        code: 500,
        data: {
          error: true,
          values: null
        }
      });
    }
  });
});

app.listen(3000, function () {
  console.log('Listen on port 3000');
});
